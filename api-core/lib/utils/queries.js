const db_queries = {
    GET_ALL_USER_TYPES: "select a.value from dictionary a where a.column_name = 'user_type_id'",
    GET_USER_TYPE: "select a.value from dictionary a where a.dictionary_id = (select user_type_id from app_user where user_id = $1 )",
    LOGIN: "select a.user_id from app_user a where a.login = $1 and a.password = $2",
    DELETE_ACCOUNT: "update app_user set status_id = (select dictionary_id from dictionary where column_name = 'STATUS_ID' and value = 'DELETED') where user_id = $1"
}

module.exports = db_queries;