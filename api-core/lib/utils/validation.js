const logger = require('../../lib/utils/logger').logger;

//Validate path parameters
const validateParams = async (params, list) => {
    logger.info('Validate path parameters');

    if (params == null || params === "") {
        throw new Error("Path parameters are undefined");
    }

    let v = JSON.stringify(params);
    let parsed = JSON.parse(v);

    for(let row of list){
        let bool = parsed.hasOwnProperty(row);
        if(!bool){
            logger.info('Parameter:  ' + row + ' is undefined');
            return false;
        }
    }

    return true;
}

module.exports = {
    validateParams
}