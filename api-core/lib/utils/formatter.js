function removeAllSpaces(result) {
    let tab = new Array()

    if (result == null) {
        return result;
    }

    let list = JSON.parse(result);

    for (let row of list) {
            let value = row.value.split(' ').join('');
            tab.push(value);
    }

    return tab;
}

module.exports = {
    removeAllSpaces,
}