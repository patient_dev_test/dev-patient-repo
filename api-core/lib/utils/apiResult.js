const responseSuccess = (result, message) => {
    return {
        statusCode: 200,
        body: JSON.stringify({
            details: `${message}`,
            result: `${result}`,
        })
    }
}

module.exports = {
    responseSuccess
}