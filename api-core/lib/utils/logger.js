// Logger
const winston = require('winston');
//const { combine, splat, json } = format;


// Create the logger
const logger = winston.createLogger({
    level: 'info',
    transports: [new winston.transports.Console(
       {
        level: 'info',
        format: winston.format.combine(
          winston.format.colorize(),
          winston.format.simple()),
        }
        )]
});


module.exports = {
    logger
};
