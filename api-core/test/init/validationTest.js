const chai = require('chai');
const validateParams = require('../../lib/utils/validation').validateParams;
const expect = require('chai').expect;
const sandbox = require('sinon').createSandbox();
const params = {
    "id" : "1",
    "login" : "Admin",
    "password" : "Test"
}

describe('Validation Test', function () {
    afterEach(function() {
        sandbox.restore();
    });
    
    it('Expects throw error if params will be null', async function () {
         await expect(validateParams()).to.be.rejectedWith(Error, "Path parameters are undefined");
    });

    it('Expects validateParams return false if wrong parameter', async function () {
        var result = await validateParams(params, ['test']);
        expect(result).to.equal(false);
    });
});