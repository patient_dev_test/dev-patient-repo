const chai = require('chai');
const app = require('../../src/handlers/app');
const secretManager = require('../../src/database/secretManager');
const connection = require('../../src/database/db');
const sandbox = require('sinon').createSandbox();
const expect = chai.expect;
const formatter = require('../../lib/utils/formatter');

//Mock db client
const mockDB = {
    query: () => {
        let object = { rows: "PATIENT" };
        return object;
    },

    end: () => {
        console.log('Conection ended');
    }
}

describe('Testing handler', function () {
    beforeEach(function () {
        sandbox.stub(formatter, "removeAllSpaces");
        sandbox.stub(secretManager, "getSecret");
        sandbox.stub(connection, "getDBClient").returns(mockDB);
    })

    afterEach(function() {
        sandbox.restore();
    });

    it('Handler expects to have status 200', async function () {
        let result = await app.getAllUserTypes();
        expect(result.statusCode).to.equal(200);
    });
});