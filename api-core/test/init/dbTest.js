const chai = require('chai');
const runQuery = require('../../src/database/db').runQuery;
const expect = require('chai').expect;
const sandbox = require('sinon').createSandbox();

const mockDB = {
    query: function () {
        let object = { rows: "PATIENT" };
        return object;
    },

    connect: function () {
        console.log('Connected');
    },

    end: function () {
        console.log('Conection ended');
    }
}

describe('DB Test', function () {
    afterEach(function() {
        sandbox.restore();
    });
    
    it('Expects user type Patient', async function () {
        var result = await runQuery(mockDB, '', '');
        expect(result).to.equal('"PATIENT"');
    });
});