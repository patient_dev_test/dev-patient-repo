const { should } = require('chai');
const chai = require('chai');
const assert = require('chai').assert;
const expect = require('chai').expect;
const secretManager = require('../../src/database/secretManager');
const chaiAsPromised = require('chai-as-promised');
const sandbox = require('sinon').createSandbox();

chai.use(chaiAsPromised);

//Mock Secrets Manager object
const mockSM = {
    getSecretValue: function ({ SecretId }) {
        {
            if (SecretId === "Test") {
                return {
                    promise: function () {
                        let object = { SecretString: "Correct secret" };
                        return object;
                    }
                };
            } else {
                return {
                    promise: function () {
                      throw new Error('Mock error');
                    }
                }
            };
        }
    }
}

describe('getSecret()', () => {
    afterEach(function() {
        sandbox.restore();
    });
    
    it('1) Expects throw error if secret name will be null', async function () {
        await expect(secretManager.getSecret(null)).to.be.rejectedWith(Error, "[SecretManager] Secret name is null");
    });

    it('2) Expects getSecret return `Correct secret`', async function () {
        let result = await secretManager.getSecret("Test", mockSM);
        expect(result).to.equal('Correct secret');
    });

    it('2) Expects secret return error if wrong secret name', async function () {
        await expect(secretManager.getSecret("Wrong", mockSM)).to.be.rejectedWith(Error, "Mock error");
    });

});