const chai = require('chai');
const dbQuery = require('../../src/services/dbQuery');
const secretManager = require('../../src/database/secretManager');
const connection = require('../../src/database/db');
const sandbox = require('sinon').createSandbox();


const expect = chai.expect;

const mockDB = {
    query: () => {
        let object = { rows: "PATIENT" };
        return object;
    },

    end: () => {
        console.log('Conection ended');
    }
}

describe('Testing dbQuery', function () {
    beforeEach(function () {
        sandbox.stub(secretManager, "getSecret");
        sandbox.stub(connection, "getDBClient").returns(mockDB);
    })

    afterEach(function() {
        sandbox.restore();
    });

    it('dbQuery expects to return PATIENT', async function () {
        let result = await dbQuery.runQuery('', '');
        expect(result).to.equal('"PATIENT"');
    });
});