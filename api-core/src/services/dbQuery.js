const secretManager = require('../database/secretManager');
const connection = require('../database/db');
const logger = require('../../lib/utils/logger').logger;


const db_connection = () => { 
    let db;
    //Get db client by secrets manager name
    const getClient = async () => {
        logger.info("Call getClient()");

        if (!db) {
            try {
                const secretValue = await secretManager.getSecret("secret_final");
                db = await connection.getDBClient(secretValue);
            } catch (e) {
                throw new Error('Could not connect to database: ' + e);
            }
        }

        return db;
    };

  // Generic function gets data by query and parameters
    const runQuery = async function (dbQuery, params) {
        try {
            logger.info('Call runQuery()');

            let client = await getClient();
            let result = await connection.runQuery(client, dbQuery, params);
            return result;
        } catch (err) {
            throw err;
        }
    };

    return {
        runQuery
    }

}


module.exports = db_connection();
