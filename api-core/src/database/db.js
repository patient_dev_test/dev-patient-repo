const { Pool, Client } = require('pg');
const logger = require('../../lib/utils/logger').logger;

//Functions sets up client credentials and connects to database
const getDBClient = async function (secretValue) {
    logger.info('[DB] getDBClient()');

    let data = getCredentials(secretValue);
    let client = new Client({
        user: data.username,
        host: data.host,
        database: data.engine,
        password: data.password,
        port: data.port
    });


    await client.connect();

    return client;
}

// Execute query and get database data
const runQuery = async function (client, queryString, queryParams) {
    logger.info('[DB] Returns data for query: ' + queryString);

    try {
        const result = await client.query(queryString, queryParams);
        return JSON.stringify(result.rows);

    } catch (e) {
        logger.error('[DB] Conection error: ' + e.stack);
        throw e;
    } finally {
        client.end();
    }
}

//Validate secrets manager value, if all nessesary parameters are correrct then returns parsed value
const getCredentials = function (secretValue) {
    logger.info('[Db] getCredentials()');

    let credentials = JSON.parse(secretValue);

    if (credentials == null || credentials == "") {
        throw new Error("Secret value is null");
    }

    if (credentials.username == null || credentials.username == "") {
        throw new Error(`User not found`);
    }

    if (credentials.host == null || credentials.host == "") {
        throw new Error(`Host not found`);
    }

    if (credentials.password == null || credentials.password == "") {
        throw new Error(`Password not found`);
    }

    if (credentials.port == null || credentials.port == "") {
        throw new Error(`Port not found`);
    }

    if (credentials.engine == null || credentials.engine == "") {
        throw new Error(`Engine not found`);
    }

    return credentials;
}

module.exports = {
    getDBClient,
    runQuery
}