const { Client } = require('pg');
const logger = require('../../lib/utils/logger').logger;

// Load the AWS SDK
const AWS = require('aws-sdk'),
    region = "eu-central-1",
    endpoint = "https://secretsmanager.eu-central-1.amazonaws.com";

// Create a Secrets Manager client
const client = new AWS.SecretsManager({
    region: region,
    endpoint: endpoint

});

// Get Secrets Manager value
const getSecret = async function (secretName,awsClient=client) {
    logger.info('[SecretManager], Getting secret');

    try {

        if (secretName == null || secretName == '') {
            throw new Error("[SecretManager] Secret name is null");
        }

        const data = await awsClient.getSecretValue({ SecretId: secretName }).promise();
      
       // console.log('data: ', data);
       // console.log('data.SecretString: ', data.SecretString);
        
        return data.SecretString;

    } catch (err) {
        logger.error(err.stack);
        throw  err;
    }
};

module.exports = {
    getSecret
}