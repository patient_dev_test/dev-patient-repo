const logger = require('../../lib/utils/logger').logger;
const db_queries = require('../../lib/utils/queries');
const db = require('../services/dbQuery');
const apiResult = require('../../lib/utils/apiResult');
const validateParams = require('../../lib/utils/validation').validateParams;
const formatter = require('../../lib/utils/formatter');

//Get all user types
const getAllUserTypes = async function (event) {
    try {
        logger.info('Call getAllUserTypes()');

        let result = await db.runQuery(db_queries.GET_ALL_USER_TYPES);  
        let value = formatter.removeAllSpaces(result);

        return apiResult.responseSuccess(value, 'Funtion getAllUserTypes succesfully executed');

    } catch (e) {
        logger.error('Error occured while login: ' + e.stack);
    }
}

//Get user type by user_id
const getUserType = async function (event) {
    try {
        logger.info('Call getUserType()');
        let params = event.pathParameters;

        validateParams(params, ['id']);

        let id = params.id;
        let result = await db.runQuery(db_queries.GET_USER_TYPE, [id]);       
        let value = formatter.removeAllSpaces(result);

        return apiResult.responseSuccess(value, 'Funtion getUserType succesfully executed');
    } catch (e) {
        logger.error('Error occured while login: ' + e.stack);
    }
}

//Get user by login and password and return user id 
const login = async (event) => {
    try {
        logger.info('Call login function');
        let params = event.pathParameters;

        validateParams(params, ['login', 'password']);

        const login = params.login;
        const password = params.password;

        let result = await db.runQuery(db_queries.LOGIN, [login, password]);

        let parsed = JSON.parse(result);

        if(parsed[0] == null){
            throw new Error('User not found. Wrong login or password');
        }

        let userId = parsed[0].user_id;

        return apiResult.responseSuccess(userId, 'Funtion getUserType succesfully executed');

    } catch (e) {
        logger.error('Error occured while login: ' + e.stack);
    }
}

//Delete user account - update user status by user id and set up status 'deleted'
const deleteAccount = async (event) => {
    try {
        logger.info('Call deleteAccount function');
        let params = event.pathParameters;
        validateParams(params, ['userId']);
        const id = params.id;

        let result = await db.runQuery(db_queries.DELETE_ACCOUNT, [id]);

        return apiResult.responseSuccess(id, 'Function getUserType executed succesfully');

    } catch (e) {
        logger.error('Error occured while login: ' + e.stack);
    }
}

module.exports = {
    login,
    deleteAccount,
    getAllUserTypes,
    getUserType
}